defmodule PotatoWeb.SummaryController do
  use PotatoWeb, :controller

  alias Potato.Samples
  alias Potato.Samples.Sample

  def summary(conn, %{"user" => user, "host" => host}) do
    #mins = Samples.video_mins_today(user)
    mins = Samples.total_video_mins_today()
    IO.inspect({"mins", mins})
    data = %{
      mins_today: mins || 0,
      daily_limit: 60,
    }
    text = Jason.encode!(data)
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, text)
  end
end
