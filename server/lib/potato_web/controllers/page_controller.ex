defmodule PotatoWeb.PageController do
  use PotatoWeb, :controller

  alias Potato.Samples

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
