defmodule PotatoWeb.SampleView do
  use PotatoWeb, :view
  alias PotatoWeb.SampleView

  def render("index.json", %{samples: samples}) do
    %{data: render_many(samples, SampleView, "sample.json")}
  end

  def render("show.json", %{sample: sample}) do
    %{data: render_one(sample, SampleView, "sample.json")}
  end

  def render("sample.json", %{sample: sample}) do
    %{
      id: sample.id,
      user: sample.user,
      data: sample.data
    }
  end
end
