defmodule Potato.DateTime do
  def now() do
    Application.get_env(:potato, :timezone)
    |> DateTime.now!()
  end

  @doc """
  Gives previous day break localtime as a UTC datetime.

  Day break is currently 15:00 local time.
  """
  def prev_morning() do
    stamp = now()
    dawn = ~T[15:00:00]

    date = if Time.compare(DateTime.to_time(stamp), dawn) == :gt do
      DateTime.to_date(stamp)
    else
      DateTime.to_date(stamp)
      |> Date.add(-1)
    end

    tz = Application.get_env(:potato, :timezone)
    DateTime.new!(date, dawn, tz)
    |> DateTime.shift_zone!("Etc/UTC")
  end
end
