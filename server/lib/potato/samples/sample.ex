defmodule Potato.Samples.Sample do
  use Ecto.Schema
  import Ecto.Changeset

  schema "samples" do
    field :user, :string
    field :tick, :integer
    field :data, :map

    timestamps()
  end

  @doc false
  def changeset(sample, attrs) do
    sample
    |> cast(attrs, [:user, :tick, :data])
    |> validate_required([:user, :tick, :data])
  end
end
