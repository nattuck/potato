defmodule Potato.Repo do
  use Ecto.Repo,
    otp_app: :potato,
    adapter: Ecto.Adapters.Postgres
end
