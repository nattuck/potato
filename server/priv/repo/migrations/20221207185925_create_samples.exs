defmodule Potato.Repo.Migrations.CreateSamples do
  use Ecto.Migration

  def change do
    create table(:samples) do
      add :user, :string, null: false
      add :tick, :integer, null: false
      add :data, :map, null: false

      timestamps()
    end

    create index(:samples, [:user, :tick], unique: true)
  end
end
