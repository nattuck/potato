# Potato

Potato monitors web usage, specifically to see how much time is spent on each
website.

Each client browser runs the userscript, which sends "samples" to the server:

 - On each initial page load for every page.
 - Every minute thereafter.
 
A sample consists of:

 - Host
 - Path
 - minute: The "unix minute" when this sample occurred. We assume
   the the site was visited for the whole minute.
 - videos: # of video tags
 - user: Configured user of browser
 - addr: IP address of client request

## Phoenix Framework

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
