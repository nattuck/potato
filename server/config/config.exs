# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
import Config

# General application configuration
config :potato,
  timezone: "America/New_York"

config :potato,
  ecto_repos: [Potato.Repo]

# Configures the endpoint
config :potato, PotatoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "e/cxKIKcuaxmNzNsVWjA7/SrrtrFXi/vEfYAzmkGUf0K2GW5p27UiEiPjuKA9TBx",
  render_errors: [view: PotatoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Potato.PubSub,
  live_view: [signing_salt: "kMlEVcFC"]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.12.18",
  default: [
    args: ~w(js/app.js --bundle --target=es2016 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
