defmodule Potato.SamplesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Potato.Samples` context.
  """

  @doc """
  Generate a sample.
  """
  def sample_fixture(attrs \\ %{}) do
    {:ok, sample} =
      attrs
      |> Enum.into(%{
        data: %{},
        user: "some user"
      })
      |> Potato.Samples.create_sample()

    sample
  end
end
