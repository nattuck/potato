defmodule PotatoWeb.SampleControllerTest do
  use PotatoWeb.ConnCase

  import Potato.SamplesFixtures

  alias Potato.Samples.Sample

  @create_attrs %{
    data: %{},
    user: "some user"
  }
  @update_attrs %{
    data: %{},
    user: "some updated user"
  }
  @invalid_attrs %{data: nil, user: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all samples", %{conn: conn} do
      conn = get(conn, Routes.sample_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create sample" do
    test "renders sample when data is valid", %{conn: conn} do
      conn = post(conn, Routes.sample_path(conn, :create), sample: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.sample_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "data" => %{},
               "user" => "some user"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.sample_path(conn, :create), sample: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update sample" do
    setup [:create_sample]

    test "renders sample when data is valid", %{conn: conn, sample: %Sample{id: id} = sample} do
      conn = put(conn, Routes.sample_path(conn, :update, sample), sample: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.sample_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "data" => %{},
               "user" => "some updated user"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, sample: sample} do
      conn = put(conn, Routes.sample_path(conn, :update, sample), sample: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete sample" do
    setup [:create_sample]

    test "deletes chosen sample", %{conn: conn, sample: sample} do
      conn = delete(conn, Routes.sample_path(conn, :delete, sample))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.sample_path(conn, :show, sample))
      end
    end
  end

  defp create_sample(_) do
    sample = sample_fixture()
    %{sample: sample}
  end
end
