defmodule Potato.SamplesTest do
  use Potato.DataCase

  alias Potato.Samples

  describe "samples" do
    alias Potato.Samples.Sample

    import Potato.SamplesFixtures

    @invalid_attrs %{data: nil, user: nil}

    test "list_samples/0 returns all samples" do
      sample = sample_fixture()
      assert Samples.list_samples() == [sample]
    end

    test "get_sample!/1 returns the sample with given id" do
      sample = sample_fixture()
      assert Samples.get_sample!(sample.id) == sample
    end

    test "create_sample/1 with valid data creates a sample" do
      valid_attrs = %{data: %{}, user: "some user"}

      assert {:ok, %Sample{} = sample} = Samples.create_sample(valid_attrs)
      assert sample.data == %{}
      assert sample.user == "some user"
    end

    test "create_sample/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Samples.create_sample(@invalid_attrs)
    end

    test "update_sample/2 with valid data updates the sample" do
      sample = sample_fixture()
      update_attrs = %{data: %{}, user: "some updated user"}

      assert {:ok, %Sample{} = sample} = Samples.update_sample(sample, update_attrs)
      assert sample.data == %{}
      assert sample.user == "some updated user"
    end

    test "update_sample/2 with invalid data returns error changeset" do
      sample = sample_fixture()
      assert {:error, %Ecto.Changeset{}} = Samples.update_sample(sample, @invalid_attrs)
      assert sample == Samples.get_sample!(sample.id)
    end

    test "delete_sample/1 deletes the sample" do
      sample = sample_fixture()
      assert {:ok, %Sample{}} = Samples.delete_sample(sample)
      assert_raise Ecto.NoResultsError, fn -> Samples.get_sample!(sample.id) end
    end

    test "change_sample/1 returns a sample changeset" do
      sample = sample_fixture()
      assert %Ecto.Changeset{} = Samples.change_sample(sample)
    end
  end
end
