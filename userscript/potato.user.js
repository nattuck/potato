// ==UserScript==
// @name        Potato
// @namespace   Violentmonkey Scripts
// @match       http*://*/*
// @grant       GM.getValue
// @version     1.7
// @author      -
// @description 8/11/2021, 1:37:50 PM
// ==/UserScript==

(function() {
  'use strict';

  let base_path = "https://bismark/api/v1";
  let sample_path = base_path + "/samples";
  let summary_path = base_path + "/summary";
  let banner = document.createElement("div");
  let retry_delay = 5000;
  let user = null;
  let clear_job = null;

  function on_blacklist(host) {
    if (host.match(/homestarrunner/)) {
      return true;
    }
    else {
      return false;
    }
  }

  function get_host_and_path() {
    let page_url = document.URL;
    let temp = page_url.replace(/^https?:\/\//, '');
    let [host, path] = temp.split('/', 2);
    path = "/" + path;
    return [host, path];
  }

  function count_videos() {
    let videos = document.getElementsByTagName('video').length;
    let [host, _path] = get_host_and_path();
    if (on_blacklist(host)) {
      videos += 1;
    }
    return videos;
  }

  function render_time(mins) {
    if (mins < 60) {
      return `${mins} mins`;
    }

    let hrs = (mins / 60).toPrecision(3);
    return `${hrs} hours`;
  }

  function render_banner_bg() {
    Object.assign(banner.style, {
      position: "fixed",
      top: "0px",
      left: "200px",
      width: "200px",
      height: "24px",
      zIndex: 9999,
      margin: "2px",
      padding: "4px",
      border: "thin dashed grey",
      backgroundColor: "#ff3",
      color: "black",
      textAlign: "center",
      fontSize: "12px",
    });
    let body = document.body;
    body.insertBefore(banner, body.childNodes[0]);
  }

  function zombocom() {
    window.location = "http://zombo.com/";
  }

  function clear_videos() {
    let videos = document.getElementsByTagName('video');

    for (let video of videos) {
      video.remove();
    }

    let audios = document.getElementsByTagName('audio');

    for (let audio of audios) {
      audio.remove();
    }

    window.setInterval(zombocom, 1000);
  }

  function render_display(data) {
    let videos = count_videos();

    let {mins_today, daily_limit} = data;
    let html = `
      <p>Video minutes today: ${mins_today} / ${daily_limit}</p>
    `;

    if (videos > 0) {
      try {
        body.removeChild(banner);
      }
      catch {
        // do nothing
      }

      render_banner_bg();
      banner.innerHTML = html;
      
      if (mins_today >= daily_limit) {
        if (clear_job) {
          clearInterval(clear_job);
          clear_job = null;
        }
        clear_job = setInterval(clear_videos, 500);
      }
    }
  }

  function update() {
    let [host, _] = get_host_and_path();
    let opts = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let path = `${summary_path}?user=${user}&host=${host}`;
    fetch(path, opts)
      .then((resp) => resp.json())
      .then((data) => {
        //console.log("summary", data);
        render_display(data);
        retry_delay = 5000;
      })
      .catch((ee) => {
        console.log("update error", ee);
        window.setTimeout(update, retry_delay);
        retry_delay *= 2;
      });
  }


  function send_sample() {
    let stamp = Date.now();
    let minute = Math.round(stamp / (60 * 1000));

    let [host, path] = get_host_and_path();
    let videos = count_videos();

    let samp = { user, host, path, minute, videos };

    let opts = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({sample: samp}),
    };
    fetch(sample_path, opts)
      .then((resp) => resp.json())
      .then((data) => {
        //console.log("user", user, data);
        update(data);
      });
  }

  function init() {
    GM.getValue("name", "anon").then((uu) => {
      user = uu;
      send_sample();
      window.setInterval(send_sample, 30 * 1000);
      console.log("potato init ran");
    });
  }

  function on_load(action) {
    if (document.readyState == "complete" ||
        document.readyState == "loaded" ||
        document.readyState == "interactive") {
      window.setTimeout(action, 2000);
    } else {
      document.addEventListener("DOMContentLoaded", action);
    }
  }

  on_load(init);
})();
