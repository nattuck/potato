// ==UserScript==
// @name        Local Potato
// @namespace   Violentmonkey Scripts
// @match       http*://*/*
// @grant       GM.getValue
// @grant       GM.setValue
// @version     0.2
// @author      Nat Tuck
// @description Initial version 2022/07/13
// ==/UserScript==

(function() {
  'use strict';

  const video_hour = 8;
  let tick_delay = 5000;

  let banner = document.createElement("div");
  let clear_job = null;
  let saw_video = false;

  function get_host_and_path() {
    let page_url = document.URL;
    let temp = page_url.replace(/^https?:\/\//, '');
    let [host, path] = temp.split('/', 2);
    path = "/" + path;
    return [host, path];
  }

  function render_banner_bg() {
    Object.assign(banner.style, {
      position: "fixed",
      top: "0px",
      left: "200px",
      width: "200px",
      height: "24px",
      zIndex: 9999,
      margin: "2px",
      padding: "4px",
      border: "thin dashed grey",
      backgroundColor: "#ff3",
      color: "black",
      textAlign: "center",
      fontSize: "12px",
    });
    let body = document.body;
    body.insertBefore(banner, body.childNodes[0]);
  }

  function render_banner(text) {
    try {
      body.removeChild(banner);
    }
    catch {
      // do nothing
    }

    let html = `<p>${text}</p>`;

    render_banner_bg();
    banner.innerHTML = html;
  }

  function zombocom() {
    window.location = "http://zombo.com/";
  }

  function clear_videos() {
    let videos = document.getElementsByTagName('video');

    for (let video of videos) {
      console.log("Remove video", video);
      video.remove();
    }

    let audios = document.getElementsByTagName('audio');

    for (let audio of audios) {
      console.log("Remove audio", audio);
      audio.remove();
    }

    //window.setInterval(zombocom, 1000);
  }

  function start_clearing() {
    if (clear_job) {
      return;
      //clearInterval(clear_job);
    }
    clear_videos();
    clear_job = setInterval(clear_videos, 2000);
  }

  function reached_video_hour() {
    let dd = new Date();
    return dd.getHours() >= video_hour;
  }

  async function enforce_rules() {
    let videos = document.getElementsByTagName('video');
    if (videos.length > 0) {
      saw_video = true;
    }
    else {
      if (!saw_video) {
        return;
      }
    }

    if (!reached_video_hour()) {
      render_banner("Not video time yet");
      start_clearing();
      return;
    }

    //console.log("video time");

    let today = (new Date()).getDate();
    let day = await GM.getValue("day", 0);

    if (day != today) {
      // New day, reset minutes.
      await GM.setValue('day', today);
      await GM.setValue('min', 0);
      await GM.setValue('count', 0);
    }

    let min = await GM.getValue("min", -1);
    let count = await GM.getValue("count", 0);
    if (count > 60) {
      render_banner("Video time is over");
      start_clearing();
      return;
    }

    let current_min = (new Date()).getMinutes();
    if (current_min != min) {
      await GM.setValue('min', current_min);
      await GM.setValue('count', count + 1);
    }

    render_banner(`Video time: ${count}/60`);
  }

  function tick() {
    enforce_rules().then(() => {
      //console.log("local potato ticked");
    });
  }

  function init() {
    tick();
    window.setInterval(tick, 5000);

    console.log("local potato started");
  }

  function on_load(action) {
    if (document.readyState == "complete" ||
        document.readyState == "loaded" ||
        document.readyState == "interactive") {
      window.setTimeout(action, 50);
    } else {
      document.addEventListener("DOMContentLoaded", action);
    }
  }

  on_load(init);
})();
