#!/bin/bash

#apt install perl-doc \
#    liblist-moreutils-perl \
#    libfile-slurp-perl \
#    libdevel-repl-perl \
#    libjson-perl \
#    suckless-tools \
#    aosd-cat \
#    xosd-bin

mkdir -p ~/.local/share/mashd
cp mashd.pl ~/.local/share/mashd

mkdir -p ~/.config/systemd/user
cp mashd.service ~/.config/systemd/user
systemctl --user daemon-reload
systemctl --user enable mashd
systemctl --user start mashd
systemctl --user restart mashd
