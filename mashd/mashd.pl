#!/usr/bin/perl
use v5.30;
use warnings FATAL => 'all';
use strict;
use autodie qw(:all);

use IO::Handle;
use Time::Piece;
use File::Path qw(make_path);
use List::MoreUtils qw(uniq);
use File::Slurp qw(read_file);
use Data::Dumper;
use POSIX qw();
use JSON;

my @potatoes = (
    'Isolated Web Co', # any intensive webpage
    'RDD Process', # video
    'steamwebhelper',
    'Tap Ninja.x86_6',
);

my @stealth = (
    'celluloid', # video player
);

sub potato_day {
    my $tt = localtime();
    return $tt->yday % 2 == 1;
}

sub is_pot {
    my ($name) = @_;
    for my $pot (@potatoes) {
        if ($name eq $pot) {
            return 1;
        }
    }
    return 0;
}

sub is_stealth {
    my ($name) = @_;
    for my $st (@stealth) {
        if ($name eq $st) {
            return 1;
        }
    }
    return 0;
}

my $LAST_HOUR = -1;

sub share_dir {
    my $HOME = $ENV{HOME};
    my $SDIR = "$HOME/.local/share/mashd";
    make_path($SDIR);
    return $SDIR;
}

sub reports_dir {
    my $tt = localtime();
    my $DATE = sprintf("%04d%02d%02d", $tt->year, $tt->mon, $tt->mday);
    my $SDIR = share_dir();
    my $RDIR = "$SDIR/reports/$DATE";
    make_path($RDIR);
    return $RDIR;
}

sub timestamp {
    my $tt = localtime();
    return $tt->strftime();
}

sub list_displays {
    my $text = `w -oush`;
    my @lines = split /\n/, $text;

    my @ys = ();

    for my $line (@lines) {
        chomp $line;
        my ($user, $tty, $disp) = split(/\s+/, $line);
        if ($disp =~ /^:/) {
            push(@ys, $disp);
        }
    }

    return uniq(sort(@ys));
}
sub list_windows {
    my @ys = ();
    for my $disp (list_displays()) {
        my $text = `bash -c 'DISPLAY=$disp wmctrl -l'`;
        my @lines = split(/\n/, $text);
        for my $line (@lines) {
            chomp($line);
            my (undef, undef, undef, $title) = split(/\s+/, $line, 4);
            push @ys, $title;
        }
    }
    return \@ys;
}

sub parse_stat {
    my ($text) = @_;

    $text =~ /^(\d+)\s+\((.*)\)\s+(.*)$/ or die "Bad stat text";
    my ($pid, $name, $rest) = ($1, $2, $3);

    my @cols = split /\s+/, $rest;

    return {
        pid  => $pid,
        name => $name,
        #rest => \@cols,
        utim => $cols[11],
    };
}

sub ps {
    my @ys = ();
    opendir my $dh, "/proc";
    while (readdir $dh) {
        next unless /^\d+$/;
        my $stat = "/proc/$_/stat";
        next unless -f $stat;
        my $text = read_file $stat;
        chomp $text;
        push(@ys, parse_stat($text));
    }
    closedir $dh;
    return @ys;
}

sub sample {
    my $hz = POSIX::sysconf( &POSIX::_SC_CLK_TCK );
    my $samt = 30;

    my @ps1 = ps();
    sleep $samt;
    my @ps2 = ps();

    my %procs = ();
    for my $stat (@ps1) {
        $procs{$stat->{pid}} = $stat;
    }

    my @ys = ();

    for my $s1 (@ps2) {
        my $s0 = $procs{$s1->{pid}} or next;

        my $pc = ($s1->{utim} - $s0->{utim}) / ($hz * $samt);
        if ($pc > 0.02) {
            $s1->{pc} = $pc;
            push @ys, $s1;
        }
    }

    return \@ys;
}

sub idle_secs {
    my @ys = ();
    for my $disp (list_displays()) {
        my $idle_ms = `xssstate -i`;
        chomp $idle_ms;
        if ($idle_ms) {
            push(@ys, $idle_ms / 1000.0);
        }
    }
    return \@ys;
}

sub report {
    my ($procs) = @_;
    my $wins = list_windows();
    my $idle = idle_secs();

    my $report = {
        procs => $procs,
        wins => $wins,
        idle => $idle,
    };

    return encode_json($report);
}

sub write_report {
    my ($report) = @_;
    my (undef, undef, $hh) = localtime();
    my $base = reports_dir();
    my $name = sprintf("report-%02d.json", $hh);
    my $path = "$base/$name";
    open my $fh, ">>", $path;
    $fh->say($report);
    close $fh;
    say "Wrote report to $path";

    if ($hh != $LAST_HOUR) {
        say "TODO: post reports";
        $LAST_HOUR = $hh;
    }
}

sub show_osd {
    my ($text) = @_;
    my $opts = "-p middle -A center -o 20 -u white -O 1";
    my $font = "-*-clean-*-*-*-*-36-120-*-*-*-*-*-*";
    system(qq{bash -c 'echo "$text" | osd_cat $opts -f "$font" -'});
}

sub enforce {
    my ($sample) = @_;
    say Dumper($sample);

    my $msg = 0;

    for my $proc (@$sample) {
        my $name = $proc->{name};
        my $pid = $proc->{pid};
        my $cpu = $proc->{pc};

        if ($name eq 'steamwebhelper') {
            say "steamwebhelper cpu = $cpu";
            next;
            #next if ($cpu < 0.2);
        }

        if (is_pot($name)) {
            if (potato_day()) {
                say "Mash potato '$name' ($pid)";
                kill('STOP', $pid);
                $msg = 1;
            }
            else {
                say "Spare potato '$name' ($pid)";
            }
        }
    }

    for my $proc (ps()) {
        my $name = $proc->{name};
        my $pid = $proc->{pid};

        if (is_stealth($name)) {
            if (potato_day()) {
                say "Kill stealth potato '$name' ($pid)";
                kill('KILL', $pid);
                $msg = 1;
            }
            else {
                say "Spare stealth potato '$name' ($pid)";
            }
        }
    }

    if ($msg) {
        show_osd("Not potato day today.");
    }
}

say "Potato Masher: startup";

while (1) {
    my $sample = sample();
    my $report = report($sample);
    write_report($report);

    my $tt = localtime();

    my $sec = $tt->sec;
    my $slp = 70 - $tt->sec;

    enforce($sample);

    say "Sec is $sec, sleep for $slp";

    sleep($slp);
}
